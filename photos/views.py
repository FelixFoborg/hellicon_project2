from django.views.generic import ListView, DetailView, UpdateView, CreateView
from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin
from django.urls import reverse

from .models import Photo


class PhotoListView(ListView):
    paginate_by = 20
    model = Photo
    template_name = "photos/photo_list.html"

    def get_paginate_by(self, queryset):
        return self.request.GET.get("paginate_by", self.paginate_by)


class PhotoDetailsView(DetailView):
    model = Photo
    template_name = "photos/photo_details.html"


class PhotoEditView(UserPassesTestMixin, UpdateView):
    model = Photo
    fields = ['description']
    template_name_suffix = "_edit_form"

    def get_success_url(self):
        return reverse("photo_details", kwargs={'pk': self.object.id})

    def test_func(self):
        return self.request.user == self.get_object().user


class PhotoUploadView(LoginRequiredMixin, CreateView):
    model = Photo
    fields = ['description', 'image']
    template_name_suffix = "_upload_form"

    def get_success_url(self):
        return reverse("photo_details", kwargs={'pk': self.object.id})

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class MyPhotoListView(LoginRequiredMixin, PhotoListView):
    template_name = "photos/my_photo_list.html"

    def get_queryset(self):
        return super().get_queryset().filter(user=self.request.user)
