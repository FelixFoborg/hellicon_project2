from django.db import models
from django.contrib.auth.models import User


class Photo(models.Model):
    description = models.CharField(max_length=160)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='images/')

    def __str__(self):
        return self.description
