from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from . import views

urlpatterns = [
    path("", views.PhotoListView.as_view(), name="home"),
    path("upload", views.PhotoUploadView.as_view(), name="upload_photo"),
    path("photos/myphotos", views.MyPhotoListView.as_view(), name="my_photo_list"),
    path("photos/<int:pk>/", views.PhotoDetailsView.as_view(), name="photo_details"),
    path("photos/<int:pk>/edit/", views.PhotoEditView.as_view(), name="edit_photo"),
    path('accounts/', include('django.contrib.auth.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
