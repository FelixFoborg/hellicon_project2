from django.test import TestCase
from django.urls import reverse

from .models import Photo


class PhotoTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.photo = Photo.objects.create(
            description="The correct description",
            user="The correct username"
        )

    def test_photo_content(self):
        self.assertEqual(self.photo.description, "The correct description")
        self.assertEqual(self.photo.user, "The correct username")

    def test_photo_listview(self):
        response = self.client.get(reverse("home"))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "The correct description")
        self.assertTemplateUsed(response, "photos/photo_list.html")
